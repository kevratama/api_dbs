var express = require('express');
var router = express.Router();

var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp
//await openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path
const axios = require('axios');

const jwt = require('jsonwebtoken');
var fs = require('fs');
var tokenA;
var decodeData;

const privkey = `-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: BCPG C# v1.6.1.0

lQOsBF1BUMABCACRUxusoZ7FxDMsUuE2y1N9t6Pnlty0AYZhbwEU7wMu/mLlrSB5
6CLJAVvDVNoe9J3XcQbrfuM4fKBWara794+yzBKRy/3j+DtxyGs1G02tEQTFmynQ
LleVZMVchVER1DnZfzkdJERYdeYdOE835cScs9GsDh1mX1Ll2UB0gXSqzFZfbczX
AbvEHUt4I5z1PYTfnixQVkxW8tzGX4zMfuBHCuLpOsrmS/+wMwYNKP3QlPYyZp0I
HJd6gotp51N9793BqLIc9GCXU/CUdNNsisovlCF3f9zYiuJoPtQ4pNmvY8Pai4xw
wSH3j2DtFN7OFGUgRWf1UUXfw0AyoC5fcTYPABEBAAH/AwMCixA5yq8qtThguGRn
FIQevgCTociNhohqCaNa7iQdD4BpXEpu3Dd4jOnkHfHSy57d540sCHiCcvKD82IU
PGWzeybIFFtAoiNnNotAG23SM6XLd96FG5tdSxDn6Cd/yNpZ6PTi/Nc2SFoBMmiy
vWuWZW2hHXsFx6Eu1ttI3Y4SdTZmqPFOpKCgLor6arTHa2Fl4Tz2m9ja4tNy529z
ZsFvBGaifF8l1FREP80gt8cXmtDfxo6R2SvhhcW/zOn/rjuwgeJ9/SIcJCNmgF8t
ddEO5mQEH3NFZIFsM7nd5TVo0v8uu4RhHlC4dv0TZA49nbtxHISFt2bDLnoTf2q0
4JiDImMjsjSHBSFJhm+daP9ATA/vID+KI86FT923WZv63vStpHCHqjdODQkltNWn
2wkgTDx9SjSTr8VKfUBXAdbaDgseo+GiN/N4d5/k0MyM1nRrbkrDtCTAOU3lscMq
KT6LYSJnu/fuoQparDDK+oxdwQ+F7Hb6eUfy9XZJjucUO8BdwYy5jUPd7clk1LXb
fLwsdfawvptenFDNTKYgqFbcPvtpes+Tep7Koyj3qm6o/zLob2WTGbmET/KdmXDj
ww7i9mLiKRorB3noaRRdRn1/KZzdKQBgOnlrJIK7bnd++vBpMOnh5lOlmZZuVcWL
9e+fS4k4DqKg5u6kYaZRdTeMyzu9NWTKSJaD1jLH6gXz2TORFqD0QuQyQIm0w1jo
eCFEAmtWjrSjpj2LBPQ0Cwdj5OOoYdZxXHKW9bxhBVGTnYbAjmwMY85K7ZMenKuE
a2K9ZR2v7UTBFlv3o0HlxXahddgxd2vd9f4k+4eRCpO8R64fBuJvOKRj63Uv5x0h
Tg5eqbMvVPEnRSRbFiF2aGk+jUEsCAaUdJePCJbBPrQHYUBhLmNvbYkBHAQQAQIA
BgUCXUFQwAAKCRBrHhiEGgz5JN6NCACKV2C0QNb/Gk44i5CFKXsJK4vY/FuM7gVu
Eb+MlQyWHMchpaMBalRM8SxOWCnb2hgkQQ8LNPMW4RfWGi5Scn9mMWXqJ3lzn397
3EFO2FSBHw3HSI0m4kYLgTR2wl9X2mNTGo6FDIDQJbYu7lR/UNUbgSfRf/i4+1dZ
GZZB9lasMqCmUJDynllS+iSJBwP5eA/DCWFdQeyHNn4uKn580e/O0ZHAyOVu9VTO
fHtKIUINFdiqkMvQxrqugIe4kvHBhNWZ/erdNolC6O1/VINmkRy9wRPBHo/Ibjt0
qMkiBmGEzVnOLbusrnC6yXyIuEeHD8raflV2XYi4T+eJ7BkwXHEE
=RORA
-----END PGP PRIVATE KEY BLOCK-----
` //encrypted private key

const passphrase = `123` 


/* GET home page. */
router.get('/', function(req, res, next) {

// Make a request for a user with a given ID
axios.get('http://localhost:3000/')
  .then(function (response) {
    // handle success

    // console.log(response.data)
    // res.send(response.data)
    tokenA = response.data;
    console.log(tokenA);

  // verify a token asymmetric
    const verifytFunction = async() => {
    fs.readFile('public.key', 'utf8', function(err, contents) {
    var publicKey = contents

    jwt.verify(tokenA, publicKey,function(err, decoded) {
      decodeData = decoded.data
      console.log(decoded.data)
      res.send(decoded.data)
      });
    });
    }

  //-------------------------------------------------
    const decryptFunction = async() => {
      const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0]
      await privKeyObj.decrypt(passphrase)
   
      const options = {
        message: await openpgp.message.readArmored(decodeData),    // parse armored message
        //publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for verification (optional)
        privateKeys: [privKeyObj]                                 // for decryption
    }

      openpgp.decrypt(options).then(plaintext => {
        console.log(plaintext.data)
        res.send(plaintext.data)
        return plaintext.data // 'Payload Data'
    })
    }

  verifytFunction()
  //decryptFunction()


  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
});

module.exports = router;
