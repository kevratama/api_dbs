var express = require('express');
var router = express.Router();

var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp
//await openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path
const jwt = require('jsonwebtoken');
var fs = require('fs');

var options, encrypted;
 
options = {
    message: openpgp.message.fromBinary(new Uint8Array([0x01, 0x01, 0x01])), // input as Message object
    passwords: ['secret stuff'],                                             // multiple passwords possible
    armor: false                                                             // don't ASCII armor (for Uint8Array output)
};
 

const pubkey = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBF1BUMABCACRUxusoZ7FxDMsUuE2y1N9t6Pnlty0AYZhbwEU7wMu/mLlrSB5
6CLJAVvDVNoe9J3XcQbrfuM4fKBWara794+yzBKRy/3j+DtxyGs1G02tEQTFmynQ
LleVZMVchVER1DnZfzkdJERYdeYdOE835cScs9GsDh1mX1Ll2UB0gXSqzFZfbczX
AbvEHUt4I5z1PYTfnixQVkxW8tzGX4zMfuBHCuLpOsrmS/+wMwYNKP3QlPYyZp0I
HJd6gotp51N9793BqLIc9GCXU/CUdNNsisovlCF3f9zYiuJoPtQ4pNmvY8Pai4xw
wSH3j2DtFN7OFGUgRWf1UUXfw0AyoC5fcTYPABEBAAG0B2FAYS5jb22JARwEEAEC
AAYFAl1BUMAACgkQax4YhBoM+STejQgAildgtEDW/xpOOIuQhSl7CSuL2PxbjO4F
bhG/jJUMlhzHIaWjAWpUTPEsTlgp29oYJEEPCzTzFuEX1houUnJ/ZjFl6id5c59/
e9xBTthUgR8Nx0iNJuJGC4E0dsJfV9pjUxqOhQyA0CW2Lu5Uf1DVG4En0X/4uPtX
WRmWQfZWrDKgplCQ8p5ZUvokiQcD+XgPwwlhXUHshzZ+Lip+fNHvztGRwMjlbvVU
znx7SiFCDRXYqpDL0Ma6roCHuJLxwYTVmf3q3TaJQujtf1SDZpEcvcETwR6PyG47
dKjJIgZhhM1Zzi27rK5wusl8iLhHhw/K2n5Vdl2IuE/niewZMFxxBA==
=RJB2
-----END PGP PUBLIC KEY BLOCK-----`

const privkey = `-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: BCPG C# v1.6.1.0

lQOsBF1BUMABCACRUxusoZ7FxDMsUuE2y1N9t6Pnlty0AYZhbwEU7wMu/mLlrSB5
6CLJAVvDVNoe9J3XcQbrfuM4fKBWara794+yzBKRy/3j+DtxyGs1G02tEQTFmynQ
LleVZMVchVER1DnZfzkdJERYdeYdOE835cScs9GsDh1mX1Ll2UB0gXSqzFZfbczX
AbvEHUt4I5z1PYTfnixQVkxW8tzGX4zMfuBHCuLpOsrmS/+wMwYNKP3QlPYyZp0I
HJd6gotp51N9793BqLIc9GCXU/CUdNNsisovlCF3f9zYiuJoPtQ4pNmvY8Pai4xw
wSH3j2DtFN7OFGUgRWf1UUXfw0AyoC5fcTYPABEBAAH/AwMCixA5yq8qtThguGRn
FIQevgCTociNhohqCaNa7iQdD4BpXEpu3Dd4jOnkHfHSy57d540sCHiCcvKD82IU
PGWzeybIFFtAoiNnNotAG23SM6XLd96FG5tdSxDn6Cd/yNpZ6PTi/Nc2SFoBMmiy
vWuWZW2hHXsFx6Eu1ttI3Y4SdTZmqPFOpKCgLor6arTHa2Fl4Tz2m9ja4tNy529z
ZsFvBGaifF8l1FREP80gt8cXmtDfxo6R2SvhhcW/zOn/rjuwgeJ9/SIcJCNmgF8t
ddEO5mQEH3NFZIFsM7nd5TVo0v8uu4RhHlC4dv0TZA49nbtxHISFt2bDLnoTf2q0
4JiDImMjsjSHBSFJhm+daP9ATA/vID+KI86FT923WZv63vStpHCHqjdODQkltNWn
2wkgTDx9SjSTr8VKfUBXAdbaDgseo+GiN/N4d5/k0MyM1nRrbkrDtCTAOU3lscMq
KT6LYSJnu/fuoQparDDK+oxdwQ+F7Hb6eUfy9XZJjucUO8BdwYy5jUPd7clk1LXb
fLwsdfawvptenFDNTKYgqFbcPvtpes+Tep7Koyj3qm6o/zLob2WTGbmET/KdmXDj
ww7i9mLiKRorB3noaRRdRn1/KZzdKQBgOnlrJIK7bnd++vBpMOnh5lOlmZZuVcWL
9e+fS4k4DqKg5u6kYaZRdTeMyzu9NWTKSJaD1jLH6gXz2TORFqD0QuQyQIm0w1jo
eCFEAmtWjrSjpj2LBPQ0Cwdj5OOoYdZxXHKW9bxhBVGTnYbAjmwMY85K7ZMenKuE
a2K9ZR2v7UTBFlv3o0HlxXahddgxd2vd9f4k+4eRCpO8R64fBuJvOKRj63Uv5x0h
Tg5eqbMvVPEnRSRbFiF2aGk+jUEsCAaUdJePCJbBPrQHYUBhLmNvbYkBHAQQAQIA
BgUCXUFQwAAKCRBrHhiEGgz5JN6NCACKV2C0QNb/Gk44i5CFKXsJK4vY/FuM7gVu
Eb+MlQyWHMchpaMBalRM8SxOWCnb2hgkQQ8LNPMW4RfWGi5Scn9mMWXqJ3lzn397
3EFO2FSBHw3HSI0m4kYLgTR2wl9X2mNTGo6FDIDQJbYu7lR/UNUbgSfRf/i4+1dZ
GZZB9lasMqCmUJDynllS+iSJBwP5eA/DCWFdQeyHNn4uKn580e/O0ZHAyOVu9VTO
fHtKIUINFdiqkMvQxrqugIe4kvHBhNWZ/erdNolC6O1/VINmkRy9wRPBHo/Ibjt0
qMkiBmGEzVnOLbusrnC6yXyIuEeHD8raflV2XYi4T+eJ7BkwXHEE
=RORA
-----END PGP PRIVATE KEY BLOCK-----` //encrypted private key

const passphrase = `123` //what the privKey is encrypted with
 

/* GET home page. */
router.get('/', function(req, res, next) {

  const encryptFunction = async() => {
    const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0]
    await privKeyObj.decrypt(passphrase)
 
    const options = {
        message: openpgp.message.fromText('Payload Data'),       // input as Message object
        publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for encryption
        privateKeys: [privKeyObj]                                 // for signing (optional)
    }
 
    openpgp.encrypt(options).then(ciphertext => {
      encrypted = ciphertext.data // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
      //res.status(200).send(encrypted)
      console.log(encrypted)
      return encrypted
    })
  } 

  
  //-----------------------------------------------------
const signFunction = async() => {
  // sign with RSA SHA256
  fs.readFile('private.key', 'utf8', function(err, contents) {
  var privateKey = contents

  jwt.sign({encrypted}, privateKey, { algorithm: 'RS256' }, function(err, token) {
    //tokenA = token
    res.status(200).send(token)   
    });
  });
  }

  encryptFunction()
  signFunction()

});

module.exports = router;
